<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserFollowerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_follower', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(User::class, 'following_id')->constrained('users');
            $table->foreignIdFor(User::class, 'follower_id')->constrained('users');
            $table->timestamps();

            $table->unique(['following_id', 'follower_id']);
            $table->index(['following_id', 'follower_id']);
            $table->index(['follower_id', 'following_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_follower');
    }
}
