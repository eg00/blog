<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $users = User::factory()->count(10)->create();
        $users->map(fn ($user) => $user->followings()->sync($users->random(random_int(1,5))->pluck('id')));
    }
}
