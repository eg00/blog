<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::all()->each(function ($user) {
            Post::factory()
                ->count(random_int(2, 20))->state(['author_id' => $user->id])
                ->create()
                ->each(fn($post) => $post->categories()
                    ->sync(Category::query()->inRandomOrder()->take(random_int(1, 3))->pluck('id')));
        });
    }
}
