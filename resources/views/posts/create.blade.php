@extends('layouts.app')

@section('content')
    <div class="container mx-auto px-4">
        <div class="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-lg -mt-64">
            <div class="px-6">
                <div class="flex flex-wrap justify-start">
                    <div class="w-full lg:w-2/12 px-4 -mb-12">
                        <div class="py-6 px-3 mt-32 sm:mt-0">
                            <a href="{{ url()->previous() }}"
                               class="w-full block ring-2 ring-indigo-500 active:bg-pink-600 px-2 py-1 bg-white text-center text-black hover:shadow-md hover:bg-indigo-500 hover:text-white shadow rounded outline-none focus:outline-none sm:mr-2 mb-1"
                               type="button">
                                Back
                            </a>
                        </div>
                    </div>
                </div>
                <div class="mt-12 px-6">
                    <div class="md:grid md:grid-cols-3 md:gap-6">

                        <div class="mt-5 md:mt-0 md:col-span-3">
                            <form action="{{route('post.store')}}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="shadow sm:rounded-md sm:overflow-hidden">
                                    <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                                        <div class="grid grid-cols-3 gap-6">


                                            <div class="col-span-3 sm:col-span-2">
                                                <label for="title"
                                                                                         class="block text-sm font-medium text-gray-700">
                                                    Title
                                                </label>
                                                <div class="mt-1 flex rounded-md shadow-sm">
                                                    <input type="text"
                                                           name="title"
                                                           id="title"
                                                           value="{{old($post->title)}}"
                                                           class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded sm:text-sm border-gray-300">
                                                </div>
                                            </div>


                                            <div class="col-span-3 sm:col-span-2">
                                                <label for="img_url"
                                                       class="block text-sm font-medium text-gray-700">
                                                    Image url
                                                </label>
                                                <div class="mt-1 flex rounded-md shadow-sm">
                                                      <span
                                                          class="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 text-sm">
                                                            <i class="fas fa-image"></i>
                                                      </span>
                                                    <input type="text" name="img_url" id="img_url"
                                                           class="focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300"
                                                           value="https://picsum.photos/640/400/?random">
                                                </div>
                                            </div>
                                        </div>

                                        <div>
                                            <label for="body" class="block text-sm font-medium text-gray-700">
                                                Post body
                                            </label>
                                            <div class="mt-1">
                                                <textarea id="body" name="body" oninput="auto_grow(this)" rows="32"
                                                          class="max-h-full h-32  shadow-sm focus:ring-indigo-500 focus:border-indigo-500 mt-1 block w-full sm:text-sm border-gray-300 rounded-md"
                                                          placeholder="you@example.com">
                                                           {{old($post->body)}}
                                                </textarea>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                                        <button type="submit"
                                                class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
