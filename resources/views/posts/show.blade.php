@extends('layouts.app')

@section('content')
    <div class="container mx-auto px-4">
        <div class="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-xl rounded-lg -mt-64">
            <div class="px-6">
                <div class="flex flex-wrap justify-start">
                    <div class="w-full lg:w-2/12 px-4 -mb-12">
                        <div class="py-6 px-3 mt-32 sm:mt-0">
                            <a href="{{ url()->previous() }}"
                               class="w-full block ring-2 ring-indigo-500 active:bg-pink-600 px-2 py-1 bg-white text-center text-black hover:shadow-md hover:bg-indigo-500 hover:text-white shadow rounded outline-none focus:outline-none sm:mr-2 mb-1"
                               type="button">
                                Back
                            </a>
                        </div>
                    </div>
                </div>
                <div class="text-center mt-12"><h3
                        class="text-4xl font-semibold leading-normal mb-2 text-gray-800 mb-2">
                        {{$post->title}}
                    </h3>
                    <div class="mb-2 text-gray-700 mt-10">
                        {{$post->author->name}}
                    </div>
                </div>
                <div class="mt-10 py-10 border-t border-gray-300 text-center">
                    <div class="flex flex-wrap justify-center">
                        <div class="w-full lg:w-9/12 px-4">
                            <p class="mb-4 text-lg leading-relaxed text-gray-800">
                                {{$post->body}}
                            </p></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
