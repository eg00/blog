@extends('layouts.app')

@section('content')
    <div class="container mx-auto px-4">
        <div class="flex flex-wrap items-center py-5 mt-32 bg-white overflow-hidden shadow sm:rounded-t-lg ">
            @foreach($posts as $post)
                <div
                    class="w-full sm:w-6/12 lg:w-4/12 px-4 mr-auto ml-auto opacity-75 transform transition duration-500 hover:opacity-100">
                    <div
                        class="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded-lg bg-white"
                    >
                        <a href="{{route('post.show', $post->slug)}}">
                            <img
                                alt="{{$post->title}}"
                                src="{{$post->img_url.'='.$post->id}}"
                                class="w-full align-middle rounded-t-lg"
                            />
                        </a>
                        <blockquote class="relative p-8 pt-0 mb-4">
                            <svg
                                preserveAspectRatio="none"
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 583 95"
                                class="absolute left-0 w-full block"
                                style="height: 95px; top: -94px;"
                            >
                                <polygon
                                    points="584,95 -30,95  583,65"
                                    class="text-white fill-current"
                                ></polygon>
                            </svg>
                            <a href="{{route('post.show', $post->slug)}}">
                                <h4 class="text-xl font-bold text-black text-right">
                                    {{$post->title}}
                                </h4>
                            </a>
                            <div class="flex align-bottom flex-col leading-none">
                                <div class="flex flex-row justify-between items-center">
                                    <a class="flex items-center no-underline hover:underline text-black" href="#">
                                        <img alt="{{$post->author->name}}" class="block rounded-full"
                                             src="https://i.pravatar.cc/50?u={{$post->author->id}}">
                                        <span class="ml-2 text-sm"> {{$post->author->name}} </span>
                                    </a>
                                </div>
                            </div>
                            <p class="text-md font-light mt-2 text-black">
                                {{\Illuminate\Support\Str::limit($post->body, 150)}}
                            </p>
                        </blockquote>
                        <ul class="flex flex-row p-4 pt-0 text-gray-600 flex-wrap">
                            @foreach($post->categories as $category)
                                <li class="py-1">
                                    <div
                                        class="transition duration-300 ease-in-out rounded-2xl mr-1 px-2 py-1 hover:bg-blue-200 text-gray-500 hover:text-gray-800">
                                        <a class=""
                                           href="{{route('category.show', $category->slug)}}"
                                        >
                                            {{$category->title}}</a>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="bg-white px-4 py-3  justify-between border-t border-gray-200 sm:px-6 sm:rounded-b-lg">
            {{ $posts->links() }}
        </div>


    </div>
@endsection
