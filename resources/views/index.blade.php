<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="theme-color" content="#000000"/>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link
        rel="stylesheet"
        href="{{asset('css/fonts.css')}}"
    />
    <link
        rel="stylesheet"
        href="{{asset('css/app.css')}}"
    />

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <title>Landing | Tailwind Starter Kit by Creative Tim</title>
</head>
<body class="text-gray-800 antialiased">
<div id="app">
<nav
    class="top-0 absolute z-50 w-full flex flex-wrap items-center justify-between px-2 py-3 navbar-expand-lg"
>
    <div
        class="container px-4 mx-auto flex flex-wrap items-center justify-between"
    >
        <div
            class="w-full relative flex justify-between lg:w-auto lg:static lg:block lg:justify-start"
        >
            <a
                class="text-sm font-bold leading-relaxed inline-block mr-4 py-2 whitespace-no-wrap uppercase text-white"
                href="{{route('index')}}"
            >Blog</a
            >
            <button
                class="cursor-pointer text-xl leading-none px-3 py-1 border border-solid border-transparent rounded bg-transparent block lg:hidden outline-none focus:outline-none"
                type="button"
                onclick="toggleNavbar('example-collapse-navbar')"
            >
                <i class="text-white fas fa-bars"></i>
            </button>
        </div>
        <div
            class="lg:flex flex-grow items-center bg-white lg:bg-transparent lg:shadow-none hidden"
            id="example-collapse-navbar"
        >
            <ul class="flex flex-col lg:flex-row list-none mr-auto">
                @foreach($categories as $category)
                    <li class="flex items-center">
                        <a
                            class="lg:text-white lg:hover:text-gray-300 text-gray-800 px-3 py-4 lg:py-2 flex items-center text-xs uppercase font-bold"
                            href="{{route('category.show', $category->slug)}}"
                        >
                            {{$category->title}}</a
                        >
                    </li>
                @endforeach
            </ul>
            <div class="flex flex-col lg:flex-row list-none lg:ml-auto">
          <dropdown-menu :user="{{auth()->user() ?? '{}'}}"/>
            </div>
        </div>
    </div>
</nav>
<main>
    <div
        class="relative pt-16 pb-32 flex content-center items-center justify-center"
        style="min-height: 75vh;"
    >
        <div
            class="absolute top-0 w-full h-full bg-center bg-cover"
            style='background-image: url("https://images.unsplash.com/photo-1557804506-669a67965ba0?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=1267&amp;q=80");'
        >
          <span
              id="blackOverlay"
              class="w-full h-full absolute opacity-75 bg-black"
          ></span>
        </div>
        <div class="container relative mx-auto">
            <div class="items-center flex flex-wrap">
                <div class="w-full lg:w-6/12 px-4 ml-auto mr-auto text-center">
                    <div class="pr-12">
                        <h1 class="text-white font-semibold text-5xl">
                            Your story starts with us.
                        </h1>
                        <p class="mt-4 text-lg text-gray-300">
                            This is a simple example of a Landing Page you can build using
                            Tailwind Starter Kit. It features multiple CSS components
                            based on the Tailwindcss design system.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div
            class="top-auto bottom-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden"
            style="height: 70px; transform: translateZ(0px);"
        >
            <svg
                class="absolute bottom-0 overflow-hidden"
                xmlns="http://www.w3.org/2000/svg"
                preserveAspectRatio="none"
                viewBox="0 0 2560 100"
                x="0"
                y="0"
            >
                <polygon
                    class="text-gray-300 fill-current"
                    points="2560 0 2560 100 0 100"
                ></polygon>
            </svg>
        </div>
    </div>
    <section class="pb-20 bg-gray-300 -mt-24">
        <div class="container mx-auto px-4">
            <div class="flex flex-wrap">
                <div class="lg:pt-12 pt-6 w-full md:w-4/12 px-4 text-center">
                    <div
                        class="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg"
                    >
                        <div class="px-4 py-5 flex-auto">
                            <div
                                class="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-red-400"
                            >
                                <i class="fas fa-award"></i>
                            </div>
                            <h6 class="text-xl font-semibold">Awarded Agency</h6>
                            <p class="mt-2 mb-4 text-gray-600">
                                Divide details about your product or agency work into parts.
                                A paragraph describing a feature will be enough.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="w-full md:w-4/12 px-4 text-center">
                    <div
                        class="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg"
                    >
                        <div class="px-4 py-5 flex-auto">
                            <div
                                class="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-blue-400"
                            >
                                <i class="fas fa-retweet"></i>
                            </div>
                            <h6 class="text-xl font-semibold">Free Revisions</h6>
                            <p class="mt-2 mb-4 text-gray-600">
                                Keep you user engaged by providing meaningful information.
                                Remember that by this time, the user is curious.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="pt-6 w-full md:w-4/12 px-4 text-center">
                    <div
                        class="relative flex flex-col min-w-0 break-words bg-white w-full mb-8 shadow-lg rounded-lg"
                    >
                        <div class="px-4 py-5 flex-auto">
                            <div
                                class="text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 mb-5 shadow-lg rounded-full bg-green-400"
                            >
                                <i class="fas fa-fingerprint"></i>
                            </div>
                            <h6 class="text-xl font-semibold">Verified Company</h6>
                            <p class="mt-2 mb-4 text-gray-600">
                                Write a few lines about each one. A paragraph describing a
                                feature will be enough. Keep you user engaged!
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="flex flex-wrap items-center mt-32">
                @foreach($recent_posts as $recent_post)
                    <div class="w-full sm:w-6/12 lg:w-4/12 px-4 mr-auto ml-auto opacity-75 transform transition duration-500 hover:opacity-100">
                        <div
                            class="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded-lg bg-white"
                        >
                            <a href="{{route('post.show', $recent_post->slug)}}">
                            <img
                                alt="{{$recent_post->title}}"
                                src="{{$recent_post->img_url.'='.$recent_post->id}}"
                                class="w-full align-middle rounded-t-lg"
                            />
                            </a>
                            <blockquote class="relative p-8 pt-0 mb-4">
                                <svg
                                    preserveAspectRatio="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 583 95"
                                    class="absolute left-0 w-full block"
                                    style="height: 95px; top: -94px;"
                                >
                                    <polygon
                                        points="584,95 -30,95  583,65"
                                        class="text-white fill-current"
                                    ></polygon>
                                </svg>
                                <a href="{{route('post.show', $recent_post->slug)}}">
                                <h4 class="text-xl font-bold text-black text-right">
                                    {{$recent_post->title}}
                                </h4>
                                </a>
                                <div class="flex align-bottom flex-col leading-none">
                                    <div class="flex flex-row justify-between items-center">
                                        <a class="flex items-center no-underline hover:underline text-black" href="#">
                                            <img alt="{{$recent_post->author->name}}" class="block rounded-full"
                                                 src="https://i.pravatar.cc/50?u={{$recent_post->author->id}}">
                                            <span class="ml-2 text-sm"> {{$recent_post->author->name}} </span>
                                        </a>
                                    </div>
                                </div>
                                <p class="text-md font-light mt-2 text-black">
                                    {{$recent_post->body_preview}}
                                </p>
                            </blockquote>
                            <ul class="flex flex-row p-4 pt-0 text-gray-600 flex-wrap">
                                @foreach($recent_post->categories as $category)
                                    <li class="py-1">
                                        <div
                                            class="transition duration-300 ease-in-out rounded-2xl mr-1 px-2 py-1 hover:bg-blue-200 text-gray-500 hover:text-gray-800">
                                            <a class=""
                                               href="{{route('category.show', $category->slug)}}"
                                            >
                                                {{$category->title}}</a>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <section class="relative">
        <div
            class="bottom-auto top-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden -mt-20"
            style="height: 80px; transform: translateZ(0px);"
        >
            <svg
                class="absolute bottom-0 overflow-hidden"
                xmlns="http://www.w3.org/2000/svg"
                preserveAspectRatio="none"
                viewBox="0 0 2560 100"
                x="0"
                y="0"
            >
                <polygon
                    class="text-white fill-current"
                    points="2560 0 2560 100 0 100"
                ></polygon>
            </svg>
        </div>
    </section>
    <section class="pt-20 pb-48">
        <div class="container mx-auto px-4">
            <div class="flex flex-wrap justify-center text-center mb-24">
                <div class="w-full lg:w-6/12 px-4">
                    <h2 class="text-4xl font-semibold">Here are our heroes</h2>
                    <p class="text-lg leading-relaxed m-4 text-gray-600">
                        According to the National Oceanic and Atmospheric
                        Administration, Ted, Scams, NSIDClad scientist, puts the
                        potentially record maximum.
                    </p>
                </div>
            </div>
            <div class="flex flex-wrap">
                @foreach($authors as $author)
                <div class="w-full md:w-6/12 lg:w-3/12 lg:mb-0 mb-12 px-4">
                    <div class="px-6">
                        <img
                            alt="..."
                            src="https://i.pravatar.cc/120?u={{$author->id}}"
                            class="shadow-lg rounded-full max-w-full mx-auto transform transition duration-500 hover:scale-120"
                            style="width: 120px;"
                        />
                        <div class="pt-6 text-center">
                            <h5 class="text-xl font-bold">{{$author->name}}</h5>
                            <p class="mt-1 text-sm text-gray-500 uppercase font-semibold">
                                ...
                            </p>
                            <div class="mt-6">
                                <button
                                    class="bg-pink-500 text-white w-8 h-8 rounded-full outline-none focus:outline-none mr-1 mb-1"
                                    type="button"
                                >
                                    <i class="fab fa-dribble"></i></button
                                >
                                <button
                                    class="bg-red-600 text-white w-8 h-8 rounded-full outline-none focus:outline-none mr-1 mb-1"
                                    type="button"
                                >
                                    <i class="fab fa-google"></i></button
                                >
                                <button
                                    class="bg-blue-400 text-white w-8 h-8 rounded-full outline-none focus:outline-none mr-1 mb-1"
                                    type="button"
                                >
                                    <i class="fab fa-twitter"></i></button
                                >
                                <button
                                    class="bg-gray-800 text-white w-8 h-8 rounded-full outline-none focus:outline-none mr-1 mb-1"
                                    type="button"
                                >
                                    <i class="fab fa-instagram"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                    @endforeach
            </div>
        </div>
    </section>


</main>
<footer class="relative bg-gray-300 pt-8 pb-6">
    <div
        class="bottom-auto top-0 left-0 right-0 w-full absolute pointer-events-none overflow-hidden -mt-20"
        style="height: 80px; transform: translateZ(0px);"
    >
        <svg
            class="absolute bottom-0 overflow-hidden"
            xmlns="http://www.w3.org/2000/svg"
            preserveAspectRatio="none"
            viewBox="0 0 2560 100"
            x="0"
            y="0"
        >
            <polygon
                class="text-gray-300 fill-current"
                points="2560 0 2560 100 0 100"
            ></polygon>
        </svg>
    </div>
    <div class="container mx-auto px-4">
        <div class="flex flex-wrap">
            <div class="w-full lg:w-6/12 px-4">
                <h4 class="text-3xl font-semibold">Let's keep in touch!</h4>
                <h5 class="text-lg mt-0 mb-2 text-gray-700">
                    Find us on any of these platforms, we respond 1-2 business days.
                </h5>
                <div class="mt-6">
                    <button
                        class="bg-white text-blue-400 shadow-lg font-normal h-10 w-10 items-center justify-center align-center rounded-full outline-none focus:outline-none mr-2 p-3"
                        type="button"
                    >
                        <i class="flex fab fa-twitter"></i></button
                    >
                    <button
                        class="bg-white text-blue-600 shadow-lg font-normal h-10 w-10 items-center justify-center align-center rounded-full outline-none focus:outline-none mr-2 p-3"
                        type="button"
                    >
                        <i class="flex fab fa-facebook-square"></i></button
                    >
                    <button
                        class="bg-white text-pink-400 shadow-lg font-normal h-10 w-10 items-center justify-center align-center rounded-full outline-none focus:outline-none mr-2 p-3"
                        type="button"
                    >
                        <i class="flex fab fa-dribble"></i></button
                    >
                    <button
                        class="bg-white text-gray-900 shadow-lg font-normal h-10 w-10 items-center justify-center align-center rounded-full outline-none focus:outline-none mr-2 p-3"
                        type="button"
                    >
                        <i class="flex fab fa-github"></i>
                    </button>
                </div>
            </div>
            <div class="w-full lg:w-6/12 px-4">
                <div class="flex flex-wrap items-top mb-6">
                    <div class="w-full lg:w-4/12 px-4 ml-auto">
                <span
                    class="block uppercase text-gray-600 text-sm font-semibold mb-2"
                >Useful Links</span
                >
                        <ul class="list-unstyled">
                            <li>
                                <a
                                    class="text-gray-700 hover:text-gray-900 font-semibold block pb-2 text-sm"
                                    href="https://www.creative-tim.com/presentation"
                                >About Us</a
                                >
                            </li>
                            <li>
                                <a
                                    class="text-gray-700 hover:text-gray-900 font-semibold block pb-2 text-sm"
                                    href="https://blog.creative-tim.com"
                                >Blog</a
                                >
                            </li>
                            <li>
                                <a
                                    class="text-gray-700 hover:text-gray-900 font-semibold block pb-2 text-sm"
                                    href="https://www.github.com/creativetimofficial"
                                >Github</a
                                >
                            </li>
                            <li>
                                <a
                                    class="text-gray-700 hover:text-gray-900 font-semibold block pb-2 text-sm"
                                    href="https://www.creative-tim.com/bootstrap-themes/free"
                                >Free Products</a
                                >
                            </li>
                        </ul>
                    </div>
                    <div class="w-full lg:w-4/12 px-4">
                <span
                    class="block uppercase text-gray-600 text-sm font-semibold mb-2"
                >Other Resources</span
                >
                        <ul class="list-unstyled">
                            <li>
                                <a
                                    class="text-gray-700 hover:text-gray-900 font-semibold block pb-2 text-sm"
                                    href="https://github.com/creativetimofficial/argon-design-system/blob/master/LICENSE.md"
                                >MIT License</a
                                >
                            </li>
                            <li>
                                <a
                                    class="text-gray-700 hover:text-gray-900 font-semibold block pb-2 text-sm"
                                    href="https://creative-tim.com/terms"
                                >Terms &amp; Conditions</a
                                >
                            </li>
                            <li>
                                <a
                                    class="text-gray-700 hover:text-gray-900 font-semibold block pb-2 text-sm"
                                    href="https://creative-tim.com/privacy"
                                >Privacy Policy</a
                                >
                            </li>
                            <li>
                                <a
                                    class="text-gray-700 hover:text-gray-900 font-semibold block pb-2 text-sm"
                                    href="https://creative-tim.com/contact-us"
                                >Contact Us</a
                                >
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <hr class="my-6 border-gray-400"/>
        <div
            class="flex flex-wrap items-center md:justify-between justify-center"
        >
            <div class="w-full md:w-4/12 px-4 mx-auto text-center">
                <div class="text-sm text-gray-600 font-semibold py-1">
                    Copyright © {{date("Y")}}
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
</body>
<script>
    function toggleNavbar(collapseID) {
        document.getElementById(collapseID).classList.toggle("hidden");
        document.getElementById(collapseID).classList.toggle("block");
    }
</script>
</html>
