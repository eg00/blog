<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'body_preview' => $this->body_preview,
            'img_url' => $this->img_url,
            'reading_time' => $this->reading_time,
            'author' => $this->whenLoaded('author'),
            'created_at_human' => $this->created_at->diffForHumans(),
        ];
    }
}
