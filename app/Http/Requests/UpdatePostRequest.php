<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required_without_all:body,img_url|unique:posts,title|max:255',
            'body' => 'required_without_all:title,img_url',
            'img_url' => ['required_without_all:title,body', 'url']
        ];
    }

    public function messages()
    {
        return [
            'title.required_without_all' => '1111A title is required',
        ];
    }
}
