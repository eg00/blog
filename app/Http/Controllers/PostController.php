<?php

namespace App\Http\Controllers;

use App\Events\PostCreated;
use App\Http\Requests\TestRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Http\Resources\TestResource;
use App\Models\Post;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * @group Post management
 *
 * APIs for managing posts
 */
class PostController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @unauthenticated
     * @param  Request  $request
     * @return JsonResponse|Response
     */
    public function index(Request $request)
    {
        $posts = Post::query()->select(['id'])->paginate(12);
        if ($request->wantsJson()) {
            return TestResource::collection($posts);
        }
        return response()->view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return response()->view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @authenticated
     * @bodyParam title string required The post title.
     * @bodyParam body string required The post body.
     * @bodyParam img_url string The post image url.
     * @param  Request  $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {


        $validated = $request->validate([
            'title' => 'required|unique:posts,title|max:255',
            'body' => 'required',
            'img_url' => ['nullable', 'url']
        ]);

        $request->user()->posts()->create($validated);

//        PostCreated::dispatch($request->user());

        return \response()->json(['ok']);
    }

    /**
     * Display the specified resource.
     *
     * @urlParam post string required The ID or Slug of the post. Example: 1
     *
     * @response {
     *  "id": 4,
     *  "name": "Jessica Jones",
     *  "roles": ["admin"]
     * }
     *
     *
     * @param  Post  $post
     * @return JsonResponse|Response
     */
    public function show(Post $post, Request $request): JsonResponse|Response
    {
        if ($request->wantsJson()) {
            return response()->json(['data' => $post]);
        }
        return response()->view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Post  $post
     * @return Response
     */
    public function edit(Post $post): Response
    {
        return response()->view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @authenticated
     * @urlParam post string required The post id or slug. Example: 1
     * @bodyParam title string The post title.
     * @bodyParam body string The post body.
     * @bodyParam img_url string The post image url.
     * @param  Request  $request
     * @param  Post  $post
     * @return JsonResponse|RedirectResponse
     */
    public function update(UpdatePostRequest $request, Post $post): JsonResponse|RedirectResponse
    {

        $validated = $request->validated();

        $post->update($validated);

        if ($request->wantsJson()) {
            return response()->json(['data' => $post]);
        }
        return redirect()->route('post.show', $post->slug);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Post  $post
     * @return Response
     */
    public function destroy(Post $post)
    {
        //
    }
}
