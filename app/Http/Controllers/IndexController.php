<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {

        $recent_posts = Post::query()->with(['author', 'categories'])->orderBy('created_at')->take(12)->get();
        $authors = User::query()->inRandomOrder()->take(4)->get();

        return response()->view('index',
            [
                'recent_posts' => $recent_posts,
                'authors' => $authors,
                ]);
    }
}
