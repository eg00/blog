<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Host;
use Illuminate\Http\Request;

class HostController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  Host  $host
     * @return Host
     */
    public function __invoke(Host $host): Host
    {
        return $host;
    }
}
