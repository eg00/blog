<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Laravolt\Avatar\Avatar;

class User extends Authenticatable
{
    use HasFactory, Notifiable;
    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return BelongsToMany
     */
    public function followers()
    {
        return $this->belongsToMany(
            __CLASS__,
            'user_follower',
            'following_id',
            'follower_id'
        )->withTimestamps();
    }

    /**
     * @return BelongsToMany
     */
    public function followings()
    {
        return $this->belongsToMany(
            __CLASS__,
            'user_follower',
            'follower_id',
            'following_id'
        )->withTimestamps();
    }

    public function posts()
    {
       return $this->hasMany(Post::class, 'author_id');
    }
}
