<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Str;

/**
 * Class Post
 * @package App\Models
 * @method paginate(int $count)

 */
class Post extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'body', 'img_url'];

    protected $appends = ['body_preview'];

    public function resolveRouteBinding($value, $field = null): ?Model
    {
        return $this->where('slug', $value)->orWhere($field ?? $this->getRouteKeyName(), $value)->firstOrFail();
    }
    /**
     * @return BelongsToMany
     */
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class, 'post_category');
    }

    /**
     * @return BelongsTo
     */
    public function author(): BelongsTo
    {
        return $this->belongsTo(User::class,  'author_id');
    }

    public function getBodyPreviewAttribute()
    {
        return Str::limit($this->body, 150);
    }
}
