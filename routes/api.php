<?php

use App\Http\Controllers\API\HostController;
use App\Http\Controllers\Auth\AuthenticatedTokenController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

Route::get('host/{host}', HostController::class);

Route::post('/login', [AuthenticatedTokenController::class, 'store'])
    ->middleware('guest');

Route::middleware('auth:sanctum')->group(
    function () {
        /**
         * @authenticated
         */
        Route::get('/user', fn (Request $request) => $request->user());
    }
);

Route::middleware('auth:sanctum')->group(function() {

    Route::get('users', [UserController::class,'index']);

    Route::apiResource('posts', PostController::class);
    Route::get('posts', [PostController::class, 'index'])->withoutMiddleware('auth:sanctum');
    Route::get('posts/{post}', [PostController::class, 'show'])->withoutMiddleware('auth:sanctum');

});

