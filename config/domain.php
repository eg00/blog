<?php

return [
  'env_stub' => '.env',
  'storage_dirs' => [
    'app' => [
      'public' => [
      ],
    ],
    'framework' => [
      'cache' => [
      ],
      'testing' => [
      ],
      'sessions' => [
      ],
      'views' => [
      ],
    ],
    'logs' => [
    ],
  ],
  'domains' => [
    'app1.blog.test' => 'app1.blog.test',
    'app2.blog.test' => 'app2.blog.test',
  ],
 ];